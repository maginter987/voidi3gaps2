#!/bin/sh
pacman -Syyu
pacman -S vi --noconfirm
pacman -S vim --noconfirm
pacman -S i3-gaps --noconfirm
pacman -S fish --noconfirm
pacman -S xorg --noconfirm
pacman -S xorg-xinit --noconfirm
pacman -S fish --noconfirm
pacman -S alacritty --noconfirm
pacman -S picom --noconfirm
pacman -S dmenu --noconfirm
pacman -S qutebrowser --noconfirm
pacman -S sddm-runit --noconfirm
pacman -S i3status --noconfirm
pacman -S i3blocks --noconfirm
pacman -S cmst --noconfirm
pacman -S xdg-user-dirs --noconfirm
pacman -S alacritty --noconfirm
pacman -S qutebrowser --noconfirm
pacman -S scrot --noconfirm
pacman -S lxappearance --noconfirm
pacman -S feh --noconfirm
pacman -S neofetch --noconfirm
pacman -S alsa-utils-runit --noconfirm 
pacman -S exa --noconfirm
pacman -S curl --noconfirm
pacman -S dunst --noconfirm
pacman -S pulseaudio --noconfirm
pacman -S nautilus --noconfirm
pacman -S betterlockscreen --noconfirm
pacman -S gvim --noconfirm
pacman -S ntp-runit --noconfirm
pacman -S polkit-gnome --noconfirm
pacman -S evolution evolution-ews --noconfirm
pacman -S pavucontrol --noconfirm
chsh -s /usr/bin/fish $(logname)
#pulseaudio and setup
ln -s /etc/runit/sv/alsa /run/runit/service/
ln -s /etc/runit/sv/dbus /run/runit/service/
ln -s /etc/runit/sv/ntpd /run/runit/service/
ntpdate 1.ro.pool.ntp.org
mkdir -p /home/$(logname)/Pictures/Screenshots/
cp -r /home/$(logname)/voidi3gaps2/. /home/$(logname)/
rm -rf /home/$(logname)/voidi3gaps2/
rm -rf /home/$(logname)/README.md
rm -rf /home/$(logname)/.git
#those line need to be last
chown -R $(logname) /home/$(logname)/
chgrp -R $(logname) /home/$(logname)/
chmod -R a+rwX,o-w /home/$(logname)
rm -rf /home/$(logname)/install.sh
ln -s /etc/runit/sv/sddm /run/runit/service/
