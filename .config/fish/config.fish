alias g='git'
alias gs="git status"
alias vi="vim"
alias gd='git diff'

set -x EDITOR vim
set -x PATH /usr/local/bin $PATH	
set -x PAGER less	
set -x VISUAL vim	
set -x TERMINAL alacritty	
set -x LESS -R	
set -x TERM alacritty	


theme_gruvbox
